//
//  Nutrition.swift
//  Nutrition
//
//  Created by Владислав on 7/20/21.
//
import UIKit
import Foundation

enum MealTime: String {
    case breakfast = "Breakfast"
    case lunch = "Lunch"
    case dinner = "Dinner"
    case defaultValue = ""
}

class Nutrition {
    let caloriesGoal: Int
    var eating: Int
    var burnt: Int
    var total: Int
    var time: String
    let mealTime: MealTime
    
    init(caloriesGoal: Int, eating: Int, burnt: Int, total: Int, time: String, mealTime: MealTime) {
        self.caloriesGoal = caloriesGoal
        self.eating = eating
        self.burnt = burnt
        self.total = total
        self.time = time
        self.mealTime = mealTime
    }
    
    func getEatingAmount(breakfast: Int, lunch: Int, dinner: Int) {
        self.eating = breakfast + lunch + dinner
    }
    
    func getTotalAmount(eating: Int, burnt: Int) {
        self.total = eating - burnt
    }
    
    func getDate(label: UILabel) {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        let result = formatter.string(from: date)
        label.text = result
    }
    
}
