//
//  UILabel + extension .swift
//  Nutrition
//
//  Created by Владислав on 7/24/21.
//

import UIKit

extension UILabel {
    func refontableLabel(text: String, mainFont: UIFont, secondaryFont: UIFont, spaceCount: Int?) {
        let attributedText = NSMutableAttributedString(string: text, attributes: [NSAttributedString.Key.font: mainFont])
        let space = " "
        attributedText.append(NSAttributedString(string: String(repeating: "\(space) kcal", count: spaceCount ?? Int()), attributes: [NSAttributedString.Key.font: secondaryFont]))
        self.attributedText = attributedText
    }
}
