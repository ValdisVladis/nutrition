//
//  ViewController.swift
//  Nutrition
//
//  Created by Владислав on 7/20/21.
//
import UIKit
import Charts

protocol NutritionViewControllerProtocol {
    func setupNutritionCollectionView()
    func customizeNutritionChartView()
    func setDataForLineChart()
}
class ViewController: UIViewController, ChartViewDelegate {
    //MARK: - UI
    @IBOutlet weak var nutritionChart: LineChartView!
    @IBOutlet weak var nutritionCollectionView: UICollectionView!
    @IBOutlet weak var caloriesGoalLabel: UILabel!
    @IBOutlet weak var eatingLabel: UILabel!
    @IBOutlet weak var burntLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    //MARK: - Variables
    private var eating = 0
    private var burnt = 90
    private var offset: CGFloat = 3.5
    private var nutrition = [Nutrition(caloriesGoal: 0, eating: 0, burnt: 90, total: 0, time: "", mealTime: .breakfast),
                             Nutrition(caloriesGoal: 0, eating: 0, burnt: 90, total: 0, time: "", mealTime: .lunch),
                             Nutrition(caloriesGoal: 0, eating: 0, burnt: 90, total: 0, time: "", mealTime: .dinner)
                             
    ]
    private var lineChartDataEntry = [ChartDataEntry(x: 0, y: 0),
                                      ChartDataEntry(x: 0.5, y: 0),
                                      ChartDataEntry(x: 1.65, y: 0),
                                      ChartDataEntry(x: 2.65, y: 0),
                                      ChartDataEntry(x: 4, y: 0)
                                      
    ]    
    //MARK: - Constants
    private let feed = Nutrition(caloriesGoal: 0, eating: 0, burnt: 90, total: 0, time: "", mealTime: .defaultValue )
    //    MARK: - ViewController life cycle funcs
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Nutrition"
        nutritionCollectionView.delegate = self
        nutritionCollectionView.dataSource = self
        setupNutritionCollectionView()
    }
    func showAlerController(caloriesLabel: UILabel,timeLabel: UILabel,mealTime: MealTime, complition: @escaping (Int) -> Void) {
        let alertController = UIAlertController(title: "It's time to record your progress", message: "Enter your nutrition data for \(mealTime)", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (alert) in
            let textField = alertController.textFields?.first
            if let value = textField?.text {
                self.feed.eating += Int(value) ?? Int()
                var count = Int(value) ?? Int()
                count += Int(caloriesLabel.text ?? String()) ?? Int()
                caloriesLabel.text = "\(count)"
                caloriesLabel.refontableLabel(text: "\(count)",
                                              mainFont: UIFont.systemFont(ofSize: 25, weight: .heavy), secondaryFont: UIFont.systemFont(ofSize: 23, weight: .regular), spaceCount: 1)
                self.feed.getDate(label: timeLabel)
                complition(count)
                UIView.animate(withDuration: 2) {
                    self.nutritionChart.alpha = 1
                }
                self.customizeNutritionChartView()
                self.caloriesGoalLabel.text = "\(self.feed.eating) kcal"
                let totalResult = "\(self.feed.eating - self.feed.burnt)"
                self.totalLabel.text = totalResult
                self.totalLabel.refontableLabel(text: totalResult,
                                                mainFont: UIFont.systemFont(ofSize: 30, weight: .heavy), secondaryFont: UIFont.systemFont(ofSize: 19, weight: .heavy), spaceCount: 5)
            }
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "Insert your data"
        }
        alertController.addAction(okAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.burntLabel.text = "\(self.feed.burnt)"
        nutritionChart.alpha = 0
        nutritionCollectionView.reloadData()
    }
}
//MARK: - Extensions
extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nutrition.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let nutritionCell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.reusableIdentifier, for: indexPath) as? CollectionViewCell else {return UICollectionViewCell()}
        let nutritionData = nutrition[indexPath.item]
        nutritionCell.populateCollectionViewCell(with: nutritionData)
        nutritionCell.action = {
            self.view.pinGradient(to: self.view)
            nutritionCell.pinShadow(to: nutritionCell.subview)
            nutritionCell.subview.pinGradient(to: nutritionCell.subview)
            self.showAlerController(caloriesLabel: nutritionCell.caloriesLabel,
                                    timeLabel: nutritionCell.timeLabel,
                                    mealTime: nutritionData.mealTime) { (value) in
                switch nutritionData.mealTime {
                case .breakfast:
                    self.lineChartDataEntry[1] = ChartDataEntry(x: 0.5, y: Double(value))
                case .lunch:
                    self.lineChartDataEntry[2] = ChartDataEntry(x: 1.65, y: Double(value))
                case .dinner:
                    self.lineChartDataEntry[3] = ChartDataEntry(x: 2.65, y: Double(value))
                case .defaultValue:
                    print("Failed to display a graph")
                }
            }
        }
        return nutritionCell
    }
}
extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let frame = collectionView.frame
        let size = frame.width / self.offset
        return CGSize(width: size , height: frame.size.height - 20)
    }
}

extension ViewController: NutritionViewControllerProtocol {
    func setupNutritionCollectionView() {
        self.nutritionCollectionView.register(CollectionViewCell.nib(),
                                              forCellWithReuseIdentifier:
                                                CollectionViewCell.reusableIdentifier)
    }
    func customizeNutritionChartView() {
        nutritionChart.isUserInteractionEnabled = false
        nutritionChart.delegate = self
        nutritionChart.leftAxis.enabled = false
        nutritionChart.rightAxis.enabled = false
        nutritionChart.xAxis.enabled = false
        nutritionChart.animate(yAxisDuration: 2)
        setDataForLineChart()
        
    }
    func setDataForLineChart() {
        let lineChartDataSet = LineChartDataSet(entries: self.lineChartDataEntry)
        lineChartDataSet.mode = .linear
        lineChartDataSet.drawCirclesEnabled = false
        lineChartDataSet.lineWidth = 5
        lineChartDataSet.setColor(UIColor.cyan)
        let lineChartData = LineChartData(dataSet: lineChartDataSet)
        self.nutritionChart.data = lineChartData
    }
}
