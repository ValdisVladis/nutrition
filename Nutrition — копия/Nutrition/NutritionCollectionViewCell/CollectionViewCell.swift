//
//  CollectionViewCell.swift
//  Nutrition
//
//  Created by Владислав on 7/20/21.
//

import UIKit

protocol CollectionViewCellProtocol {
    func populateCollectionViewCell(with object: Nutrition)
    var action: (()->Void)? {get set}
}

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var mealTimeLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var subview: UIView!
    
    static let reusableIdentifier = "NutritionCollectionViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "CollectionViewCell", bundle: nil)
    }
    var action: (()->Void)?
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 11
        self.layer.borderWidth = 0.2
        self.layer.borderColor = UIColor(red: 0.342, green: 0.339, blue: 0.339, alpha: 0.28).cgColor
        self.backgroundColor = #colorLiteral(red: 0.9699861407, green: 0.960067451, blue: 0.9776807427, alpha: 1)
    
    }
}
extension CollectionViewCell: CollectionViewCellProtocol {
    func populateCollectionViewCell(with object: Nutrition) {
        self.mealTimeLabel.text = object.mealTime.rawValue.uppercased()
    }
    @IBAction func addCalories(_ sender: UIButton) {
        if let buttonSender = self.action {
            buttonSender()
        }
        
    }
}
